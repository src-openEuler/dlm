Name:           dlm
Version:        4.3.0
Release:        1
License:        GPLv2 and GPLv2+ and LGPLv2+
Group:          System Environment/Kernel
Summary:        dlm control daemon and tool
URL:            https://pagure.io/dlm
BuildRequires:  glibc-kernheaders
BuildRequires:  corosynclib-devel >= 3.1.0
BuildRequires:  pacemaker-libs-devel >= 1.1.7
BuildRequires:  libxml2-devel
BuildRequires:  systemd-units
BuildRequires:  systemd-devel
Source0:        https://releases.pagure.org/dlm/%{name}-%{version}.tar.gz

Requires:       %{name}-lib = %{version}-%{release}
Requires:       corosync >= 3.1.0
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Conflicts: cman

%description
The kernel dlm requires a user daemon to control membership.

%package        lib
Summary:        Library for %{name}
Group:          System Environment/Libraries
Conflicts:      clusterlib

%description    lib
The %{name}-lib package contains the libraries needed to use the dlm
from userland applications.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}-lib = %{version}-%{release}
Conflicts:      clusterlib-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
CFLAGS+=$RPM_OPT_FLAGS make
CFLAGS+=$RPM_OPT_FLAGS make -C fence

%install
rm -rf $RPM_BUILD_ROOT
make install LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT
make -C fence install LIBDIR=%{_libdir} DESTDIR=$RPM_BUILD_ROOT

install -Dm 0644 init/dlm.service %{buildroot}%{_unitdir}/dlm.service
install -Dm 0644 init/dlm.sysconfig %{buildroot}/etc/sysconfig/dlm

%post
%systemd_post dlm.service

%preun
%systemd_preun dlm.service

%postun
%systemd_postun_with_restart dlm.service

%post lib -p /sbin/ldconfig

%postun lib -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc README.license
%{_unitdir}/dlm.service
%{_sbindir}/dlm_controld
%{_sbindir}/dlm_tool
%{_sbindir}/dlm_stonith
%{_mandir}/man8/dlm*
%{_mandir}/man5/dlm*
%{_mandir}/man3/*dlm*
%config(noreplace) %{_sysconfdir}/sysconfig/dlm

%files          lib
%defattr(-,root,root,-)
%{_prefix}/lib/udev/rules.d/*-dlm.rules
%{_libdir}/libdlm*.so.*

%files          devel
%defattr(-,root,root,-)
%{_libdir}/libdlm*.so
%{_includedir}/libdlm*.h
%{_libdir}/pkgconfig/*.pc

%changelog
* Tue Jun 11 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.3.0-1
- Update to version 4.3.0
- fix numerous dlm_controld bugs, such as update list implementation

* Mon May 13 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-9
- libdlm_lt: fix pc file

* Mon May 06 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-8
- dlm_controld: be sure we stop lockspaces before shutdown and add yaml file

* Mon Apr 29 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-7
- dlm_tool: fix missing fclose calls

* Fri Mar 08 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-6
- build: dlm_controld disable annobin plugin

* Wed Mar 06 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-5
- Revert "treewide: add -fcf-protection=full to CFLAGS"

* Tue Mar 05 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-4
- dlm_controld: update Linux kernel implementations

* Mon Mar 04 2024 laokz <zhangkai@iscas.ac.cn> - 4.2.0-3
- riscv64: remove -fcf-protection

* Fri Mar 01 2024 zouzhimin <zouzhimin@kylinos.cn> - 4.2.0-2
- dlm_controld: fix various deadcode issues

* Tue Dec 26 2023 Ge Wang <wang__ge@126.com> - 4.2.0-1
- Update to version 4.2.0

* Tue Jul 18 2023 chenchen <chen_aka_jan@163.com> - 4.1.0-2
- dlm_controld: remove unnecessary header include

* Thu May 11 2023 xuxiaojuan <xuxiaojuan@kylinos.cn> - 4.1.0-1
- Package init

